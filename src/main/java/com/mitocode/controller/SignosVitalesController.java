package com.mitocode.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mitocode.exception.ModeloNotFoundException;
import com.mitocode.model.Paciente;
import com.mitocode.model.SignoVital;
import com.mitocode.service.ISignosVitalesService;

@RestController
@RequestMapping("/signosvitales")
public class SignosVitalesController {
   
	@Autowired
	private ISignosVitalesService service;
	
	@PostMapping(produces = "application/json", consumes = "application/json")
	public ResponseEntity<SignoVital> registrar(@RequestBody SignoVital signovital) throws Exception {
		SignoVital sigvital = service.registrar(signovital);
		return new ResponseEntity<SignoVital>(sigvital, HttpStatus.CREATED);
	}
	
	@GetMapping(produces = "application/json")
	public ResponseEntity<List<SignoVital>> listar() throws Exception {
		List<SignoVital> list_signosvit = new ArrayList<SignoVital>();
		list_signosvit = service.listar();
		return new ResponseEntity<List<SignoVital>>(list_signosvit, HttpStatus.OK);
	}
	
	@GetMapping(value="/pageable", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Page<SignoVital>> listarPageable(Pageable pageable) throws Exception {
		Page<SignoVital> page_signovital = service.listarPageable(pageable);
		return new ResponseEntity<Page<SignoVital>>(page_signovital, HttpStatus.OK);
	}
	
	@PutMapping(produces = "application/json", consumes = "application/json")
	public ResponseEntity<SignoVital> modificar(@RequestBody SignoVital signovital) throws Exception {
        SignoVital sig_vital=service.modificar(signovital);
		return new ResponseEntity<SignoVital>(sig_vital, HttpStatus.OK);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> eliminar(@PathVariable("id") Integer id) throws Exception {
		SignoVital obj = service.listarPorId(id);
		
		if(obj == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		service.eliminar(id);
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<SignoVital> listarPorId(@PathVariable("id") Integer id) throws Exception {
		SignoVital obj = service.listarPorId(id);
		
		if(obj.getId() == null) {
			throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
		}
		return new ResponseEntity<SignoVital>(obj, HttpStatus.OK);
	}

}
