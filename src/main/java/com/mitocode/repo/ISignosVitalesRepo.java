package com.mitocode.repo;

import com.mitocode.model.SignoVital;

public interface ISignosVitalesRepo extends IGenericRepo<SignoVital, Integer> {

}
