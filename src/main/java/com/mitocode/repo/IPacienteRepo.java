package com.mitocode.repo;

import java.util.List;

import com.mitocode.model.Paciente;

//@Repository
public interface IPacienteRepo extends IGenericRepo<Paciente, Integer>{
	
	
	List<Paciente> findByNombresIgnoreCaseContainingOrApellidosIgnoreCaseContaining(String nombres,String apellidos);

}
