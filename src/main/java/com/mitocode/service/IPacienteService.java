package com.mitocode.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.mitocode.model.Paciente;

public interface IPacienteService extends ICRUD<Paciente, Integer>{
	
	Page<Paciente> listarPageable(Pageable pageable);
	List<Paciente> buscarCliente(String nombres,String apellidos);


}
