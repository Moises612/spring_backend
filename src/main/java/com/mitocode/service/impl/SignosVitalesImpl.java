package com.mitocode.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.mitocode.model.SignoVital;
import com.mitocode.repo.ISignosVitalesRepo;
import com.mitocode.service.ISignosVitalesService;

@Service
public class SignosVitalesImpl implements ISignosVitalesService {

	@Autowired
	private ISignosVitalesRepo repo;
	
	@Override
	public SignoVital registrar(SignoVital t) throws Exception {
		// TODO Auto-generated method stub
		return repo.save(t);
	}

	@Override
	public SignoVital modificar(SignoVital t) throws Exception {
		// TODO Auto-generated method stub
		return repo.save(t);
	}

	@Override
	public List<SignoVital> listar() throws Exception {
		// TODO Auto-generated method stub
		return repo.findAll();
	}

	@Override
	public SignoVital listarPorId(Integer id) throws Exception {
		// TODO Auto-generated method stub
		Optional<SignoVital>opt=repo.findById(id);
		return opt.isPresent()?opt.get():new SignoVital();
	}

	@Override
	public void eliminar(Integer id) throws Exception {
		// TODO Auto-generated method stub
		repo.deleteById(id);
	}

	@Override
	public Page<SignoVital> listarPageable(Pageable pageable) {
		// TODO Auto-generated method stub
		return repo.findAll(pageable);
	}

}
